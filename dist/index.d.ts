export declare class AppCompletion {
    private setup;
    private completion;
    private completionTree;
    constructor(setup?: boolean);
    removeSetup(): void;
    addCommand(command: string | string[]): void;
    addCommandList(commands: (string | string[])[]): void;
    addDefaultCommands(): void;
    init(): boolean;
    private addTreeNodes;
}
declare module '@sgtools/application' {
    interface Application {
        completion: AppCompletion;
    }
}
