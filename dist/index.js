"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppCompletion = void 0;
const application_1 = require("@sgtools/application");
const omelette = require("omelette");
class AppCompletion {
    constructor(setup = true) {
        this.setup = setup;
        this.completionTree = {};
    }
    removeSetup() {
        this.setup = false;
    }
    addCommand(command) {
        if (typeof command === 'string')
            command = [command];
        this.addTreeNodes(command);
    }
    addCommandList(commands) {
        commands.forEach(command => {
            this.addCommand(command);
        });
    }
    addDefaultCommands() {
        application_1.app.getCommands().map(command => {
            command.names.forEach(name => {
                this.addTreeNodes([name]);
            });
        });
    }
    init() {
        if (this.completionTree === {})
            return false;
        this.completion = omelette(application_1.app.name).tree(this.completionTree);
        this.completion.init();
        if (this.setup) {
            if (~process.argv.indexOf('--setup')) {
                this.completion.setupShellInitFile();
                console.log(`${application_1.app}: Completion file created`);
            }
            if (~process.argv.indexOf('--cleanup')) {
                this.completion.cleanupShellInitFile();
                console.log(`${application_1.app}: Completion file deleted`);
            }
        }
        return true;
    }
    addTreeNodes(nodes) {
        let t = this.completionTree;
        nodes.forEach(node => {
            if (!Object.keys(t).includes(node))
                t[node] = {};
            t = t[node];
        });
    }
}
exports.AppCompletion = AppCompletion;
application_1.Application.prototype.completion = new AppCompletion();
