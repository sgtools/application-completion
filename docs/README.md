@sgtools/application-completion / [Exports](modules.md)

# Welcome to @sgtools/application-completion
[![Version](https://npm.bmel.fr/-/badge/sgtools/application-completion.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/application-completion)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/application-completion/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Application framework nodejs applications
> Completion

## Install

```sh
npm install @sgtools/application-completion
```

## Usage

TODO

Code documentation can be found [here](https://gitlab.com/sgtools/application-completion/-/blob/master/docs/README.md).

## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>

## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/application-completion/issues). You can also contact the author.

## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.
