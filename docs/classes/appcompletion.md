[@sgtools/application-completion](../README.md) / [Exports](../modules.md) / AppCompletion

# Class: AppCompletion

## Hierarchy

* **AppCompletion**

## Table of contents

### Constructors

- [constructor](appcompletion.md#constructor)

### Properties

- [completion](appcompletion.md#completion)
- [completionTree](appcompletion.md#completiontree)
- [setup](appcompletion.md#setup)

### Methods

- [addCommand](appcompletion.md#addcommand)
- [addCommandList](appcompletion.md#addcommandlist)
- [addDefaultCommands](appcompletion.md#adddefaultcommands)
- [addTreeNodes](appcompletion.md#addtreenodes)
- [init](appcompletion.md#init)
- [removeSetup](appcompletion.md#removesetup)

## Constructors

### constructor

\+ **new AppCompletion**(`setup?`: *boolean*): [*AppCompletion*](appcompletion.md)

#### Parameters:

Name | Type | Default value |
------ | ------ | ------ |
`setup` | *boolean* | true |

**Returns:** [*AppCompletion*](appcompletion.md)

Defined in: index.ts:9

## Properties

### completion

• `Private` **completion**: *Instance*

Defined in: index.ts:8

___

### completionTree

• `Private` **completionTree**: TreeValue

Defined in: index.ts:9

___

### setup

• `Private` **setup**: *boolean*

Defined in: index.ts:7

## Methods

### addCommand

▸ **addCommand**(`command`: *string* \| *string*[]): *void*

#### Parameters:

Name | Type |
------ | ------ |
`command` | *string* \| *string*[] |

**Returns:** *void*

Defined in: index.ts:22

___

### addCommandList

▸ **addCommandList**(`commands`: (*string* \| *string*[])[]): *void*

#### Parameters:

Name | Type |
------ | ------ |
`commands` | (*string* \| *string*[])[] |

**Returns:** *void*

Defined in: index.ts:29

___

### addDefaultCommands

▸ **addDefaultCommands**(): *void*

**Returns:** *void*

Defined in: index.ts:36

___

### addTreeNodes

▸ `Private`**addTreeNodes**(`nodes`: *string*[]): *void*

#### Parameters:

Name | Type |
------ | ------ |
`nodes` | *string*[] |

**Returns:** *void*

Defined in: index.ts:67

___

### init

▸ **init**(): *boolean*

**Returns:** *boolean*

Defined in: index.ts:45

___

### removeSetup

▸ **removeSetup**(): *void*

**Returns:** *void*

Defined in: index.ts:17
