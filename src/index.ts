import { Application, app } from '@sgtools/application';

import * as omelette from 'omelette';

export class AppCompletion
{
    private setup: boolean;
    private completion: omelette.Instance;
    private completionTree: omelette.TreeValue;

    constructor(setup: boolean = true)
    {
        this.setup = setup;
        this.completionTree = {};
    }

    public removeSetup()
    {
        this.setup = false;
    }

    public addCommand(command: string | string[])
    {
        if (typeof command === 'string') command = [command];

        this.addTreeNodes(command);
    }

    public addCommandList(commands: (string | string[])[])
    {
        commands.forEach(command => {
            this.addCommand(command);
        });
    }

    public addDefaultCommands()
    {
        app.getCommands().map(command => {
            command.names.forEach(name => {
                this.addTreeNodes([name]);
            });
        });
    }

    public init()
    {
        if (this.completionTree === {}) return false;

        this.completion = omelette(app.name).tree(this.completionTree);
        this.completion.init();

        if (this.setup) {
            if (~process.argv.indexOf('--setup')) {
                this.completion.setupShellInitFile();
                console.log(`${app}: Completion file created`);
            }
            
            if (~process.argv.indexOf('--cleanup')) {
                this.completion.cleanupShellInitFile();
                console.log(`${app}: Completion file deleted`);
            }
        }

        return true;
    }

    private addTreeNodes(nodes: string[])
    {
        let t = this.completionTree as any;
        nodes.forEach(node => {
            if (!Object.keys(t).includes(node)) t[node] = {};
            t = t[node];
        });
    }
}

declare module '@sgtools/application'
{
    export interface Application
    {
        completion: AppCompletion
    }
}
Application.prototype.completion = new AppCompletion();




